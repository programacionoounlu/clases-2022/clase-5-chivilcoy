package ar.edu.unlu.prendas;

/**
 * Clase base base para implementar una prenda
 * @author sriccisoft
 */
public class Prenda {
	private double porcentajeDeGanancia;
	
	private double precioDeLista;
	
	protected Prenda(double precioDeLista, double porcentajeDeGanancia) throws ValorNegativoException {
		this.setPrecioDeLista(precioDeLista);
		this.setPorcentajeDeGanancia(porcentajeDeGanancia);
	}
	
	public double getPrecioDeLista() {
		return this.precioDeLista;
	}

	public double getPorcentajeDeGanancia() {
		return porcentajeDeGanancia;
	}

	public void setPorcentajeDeGanancia(double porcentajeDeGanancia) throws ValorNegativoException {
		if(porcentajeDeGanancia < 0) {
			throw new ValorNegativoException("No se admiten porcentajes de ganancias negativos. Valor recibido" + 
						String.valueOf(porcentajeDeGanancia));
		} else {
			this.porcentajeDeGanancia = porcentajeDeGanancia;
		}
	}

	public void setPrecioDeLista(double precioDeLista) {
		this.precioDeLista = precioDeLista;
	}
	
	/**
	 * Calcula el precio de venta de una prenda
	 * @return
	 */
	public double calcularPrecioDeVenta() {
		return (this.getPorcentajeDeGanancia() / 100) * 
				this.calcularCosto() + this.calcularCosto();
	}
	
	/**
	 * Calcula el costo de una prenda
	 * @return
	 */
	public double calcularCosto() {
		return this.getPrecioDeLista();
	}
	
}
