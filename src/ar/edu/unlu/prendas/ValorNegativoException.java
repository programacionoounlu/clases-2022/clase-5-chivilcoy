package ar.edu.unlu.prendas;

/**
 * Excepción producida cuando se recibe un valor negativo donde
 * no es admitido
 * @author sriccisoft
 *
 */
public class ValorNegativoException extends Exception{
	public ValorNegativoException(double valor) {
		super("No se admite un valor negativo. Valor recibido: " + 
						String.valueOf(valor));
	}
	
	public ValorNegativoException(String mensaje) {
		super(mensaje);
	}
}
