package ar.edu.unlu.prendas;

import java.util.List;

public class Venta {
	private List<Prenda> prendas;
	
	private Tarjeta tarjeta;
	
	public Venta(List<Prenda> prendas) {
		this.prendas = prendas;
	}
	
	public Venta(List<Prenda> prendas, Tarjeta tarjeta) {
		this(prendas);
		this.tarjeta = tarjeta;
	}
	
	public double calcularTotal() {
		double total = 0;
		for (Prenda prenda : prendas) {
			total += prenda.calcularPrecioDeVenta();
		}
		if(this.esConTarjeta()) {
			total -= this.tarjeta.calcularDescuento(total); 
		}
		return total;
	}
	
	public boolean esConTarjeta() {
		return this.tarjeta != null;
	}
}
