package ar.edu.unlu.prendas;

import java.util.ArrayList;
import java.util.List;

public class Test {

	public static void main(String[] args) {
		try {
			Prenda p = new Prenda(100, 10);
			List<Prenda> lista = new ArrayList<Prenda>();
			lista.add(new Camisa(100, 10, true));
			lista.add(new Remera(100, 15, 50));
			lista.add(new Sweater(100, 10));
			Venta venta = new Venta(lista, new Tarjeta(true));
			System.out.println(venta.calcularTotal());
			
		} catch (ValorNegativoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
