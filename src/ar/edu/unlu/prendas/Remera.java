package ar.edu.unlu.prendas;

public class Remera extends Prenda{
	
	private double impuesto;
	
	public Remera(double precioDeLista, double ganancia, double impuesto) throws ValorNegativoException, ValorNegativoException{
		super(precioDeLista, ganancia);
		this.setImpuesto(impuesto);
	}
	
	public void setImpuesto(double impuesto) throws ValorNegativoException {
		if(impuesto < 0) {
			throw new ValorNegativoException(impuesto);
		}
		this.impuesto = impuesto;
	}
	
	public double getImpuesto() {
		return this.impuesto;
	}	
	
	@Override
	public double calcularCosto() {
		return super.calcularCosto() + this.getImpuesto();
	}



}
