package ar.edu.unlu.prendas;

public class Tarjeta {
	
	private static final double DTO = 0.015;
	
	private boolean dorada;
	
	/**
	 * Genera una instancia de tarjeta
	 * @param esDorada true para tarjeta dorada, false en otro caso
	 */
	public Tarjeta(boolean esDorada) {
		this.dorada = esDorada;
	}

	public boolean isDorada() {
		return this.dorada;
	}
	
	public double calcularDescuento(double total) {
		double dto;
		if(this.isDorada()) {
			dto = Tarjeta.DTO * total + 100;
		}else {
			dto = 0.01 * total;
		}
		return dto;
	}

}
