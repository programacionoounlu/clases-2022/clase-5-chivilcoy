package ar.edu.unlu.prendas;

public class Sweater extends Prenda{

	public Sweater(double precioDeLista, double porcentajeDeGanancia) throws ValorNegativoException {
		super(precioDeLista, porcentajeDeGanancia);
	}
	
	@Override
	public double calcularPrecioDeVenta() {
		return super.calcularPrecioDeVenta() + 0.08 * this.getPrecioDeLista();
	}

}
