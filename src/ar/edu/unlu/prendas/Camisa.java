package ar.edu.unlu.prendas;

public class Camisa extends Prenda{
	private boolean mangaLarga;
	
	public Camisa(double precioDeLista, double porcentajeDeGanancia, boolean mangaLarga) throws ValorNegativoException {
		super(precioDeLista, porcentajeDeGanancia);
		this.setMangaLarga(mangaLarga);
	}

	public boolean isMangaLarga() {
		return mangaLarga;
	}

	private void setMangaLarga(boolean mangaLarga) {
		this.mangaLarga = mangaLarga;
	}
	
	@Override
	public double calcularPrecioDeVenta() {
		double precioVta = super.calcularPrecioDeVenta();
		if(this.isMangaLarga()) {
			precioVta += 0.05 * precioVta;
		}
		return precioVta;
	}
		
}
