

import java.util.ArrayList;
import java.util.List;

import ar.edu.unlu.prendas.Camisa;
import ar.edu.unlu.prendas.Prenda;
import ar.edu.unlu.prendas.Remera;
import ar.edu.unlu.prendas.Sweater;
import ar.edu.unlu.prendas.Tarjeta;
import ar.edu.unlu.prendas.ValorNegativoException;
import ar.edu.unlu.prendas.Venta;

public class Test {

	public static void main(String[] args) {
		try {
			List<Prenda> lista = new ArrayList<Prenda>();
			lista.add(new Camisa(100, 10, true));
			lista.add(new Remera(100, 15, 50));
			lista.add(new Sweater(100, 10));
			Venta venta = new Venta(lista, new Tarjeta(true));
			System.out.println(venta.calcularTotal());
			
		} catch (ValorNegativoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
